'''
Created on 8 dic. 2020

@author: Rafael Villaneda
'''
class BinarySearch:
    
    def busquedaBinaria(self,array,buscado):
        inicio=0
        final=len(array)-1
        
        while inicio<= final:
            puntero=(inicio+final)//2
            if buscado==array[puntero]:
                return 1
            elif buscado > array[puntero]:
                inicio=puntero+1
            else:
                final=puntero-1
        return 0
            
class hash_table:
    def __init__(self):
        self.table = [None] * 127

    # Función hash
    def Hash_func(self, value):
        key = 0
        for i in range(0,len(value)):
            key += ord(value[i])
        return key % 127

    def Insert(self, value): # Metodo para ingresar elementos
        hash = self.Hash_func(value)
        if self.table[hash] is None:
            self.table[hash] = value

    def Search(self,value): # Metodo para buscar elementos
        hash = self.Hash_func(value);
        if self.table[hash] is None:
            return None
        else:
            return (self.table[hash])

    def Remove(self,value): # Metodo para eleminar elementos
        hash = self.Hash_func(value);
        if self.table[hash] is None:
            print("No hay elementos con ese valor", value)
        else:
            print("Elemento con valor", value, "eliminado")
            self.table[hash] is None;

busqueda=BinarySearch()
vector=[1,10,11,12,13,14,15,16,17,18,19,20]
if(busqueda.busquedaBinaria(vector,10)==1):
    print("Se encontro el elemento")
else:
    print("No se encontro el elemento")
print("Busqueda Hash")
H = hash_table()
H.Insert("1")
H.Insert("2")
H.Insert("3")

print(H.Search("4"))



